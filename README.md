# Control a Two-Wheel Robot with Jolie

This project allows to control a robot able to move forward and spin on the left or on the right. The implemented control systems covers: 
1. Simple commands control via **Jolie Programming Language** code; 
2. A Web Server interface to control Mr Ugly Robot with buttons on a web page; 
3. It is also possible to control Mr Ugly Robot via a **Telegram** existing Bot (@Mr_Ugly_Robot) through the API. 
4. In the end it is possible, via the Twitter API, to twit the robot status update.

This project is not focused on the building of the Robot since I concentrated on the orchestration side, however the components and the [Fritzing](http://fritzing.org/) project can be found in the *fritzing* folder. The following scheme for the Robot is proposed:

![Fritzing scheme in *fritzing/arduinoNodeRobot.fzz*](https://bitbucket.org/saltgz/mr-ugly-robot/raw/37490607ae77d3d69fad786e9c9e48534bc2036f/fritzing/scheme.png)

## Getting Started

The project is divided in 3 main parts: 
1. Flash the proper firmware on the Arduino to control the wheels;
2. Flash the firmware for the **NodeMCU** to enable the WiFi communication with Jolie;
3. Test the configuration running the Jolie controllers.

### Prerequisites

1. The **Jolie for IoT** Interpreter - instruction and software @ http://cs.unibo.it/projects/jolie/jiot.html
2. **Arduino IDE** to flash both the **Arduino/Genuino** and the **NodeMCU** - http://arduino.cc, make sure to install the following boards and libraries once you install the IDE:
  * ESP8266 board;
  * MQTT;
  * Simple CoAP library;
  * WifiUDP;
  * ESP8266WiFi.
3. Text editor, I used [**Sublime-Text**](https://www.sublimetext.com)
4. USBt to UART Driver for **NodeMCU** are **CP2102** and can be found @ https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers

### Installing

First of all we need to flash both *arduino.ino* firmware into the Arduino, and the *nodemcu.ino* firmware into the **NodeMCU**, to accomplish that we will use the **Arduino IDE**.

1. First open *arduino* project in the **Arduino IDE**, select the **Arduino/Genuino** board,and the proper USB port and flash the firmware.
2. Open *nodeMCU* project in the **Arduino IDE**, select the **NodeMCU** board, and the proper USB port (make sure to install **CP2102** driver before!), and modify line 60 with your WiFi credentials:

```c++
WiFi.begin('<your_wifi_essid>', '<your_wifi_pwd>');
```

3. Open *interface.iol* in the *jolie* folder with you preferred text editor and modify the IP addresses for the **Mqtt Broker** --- I used [Eclipse Mosquitto�](https://mosquitto.org/) installed via [Homebrew](https://brew.sh/index_it.html).

```jolie
constants {
    Robot_coap_location = "datagram://<ip_address_of_coap_server>:5683",
    Broker_location = "socket://<ip_address_of_mosquitto>:1883",
    Web_location = "socket://localhost:9000",
    Telegram_location = "socket://api.telegram.org:443/"
}
```

## Running the tests

1. Run **simple_command.ol** in the **jolie** folder from the command line with `jolie simple_command.ol`;

2. Run **webServer.ol** in the **jolie** folder from the command line with `jolie webServer.ol`, then connect to `http://localhost:9000/index` and there you will find you web-based control system;

3. Open **telegram.ol** in the **jolie** folder and modify *line 17* to match your Bot `<secret_token>`:

```jolie
getUpdates@Telegram_Bot( { .offset = -1, .token = '<secret_token>' } )( response );
```

4. Then run it from the command line with `jolie telegram.ol`;

5. Run **twitter.ol** in the **jolie** folder and from the command line with `jolie twitter.ol <update_status>`.

### Break down into end to end tests

* `simple_command.ol` tests the controls for the robot, you should see the robot move as you told him: `go` moves it straight forward, `spin` make him turn on himself.
* `webServer.ol`
* `telegram.ol`
* `twitter.ol`

## Built With

* [Jolie](http://jolie-lang.org) - The Programming Language used for orchestration;
* [Coap Simple Library](https://github.com/hirotakaster/CoAP-simple-library) - The Arduino library for CoAP server implementation;
* [MQTT](https://www.arduinolibraries.info/libraries/mqtt) - The Arduino library for MQTT Client implementation.

## Contributing

Please read [CONTRIBUTING](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Prof. Maurizio Gabbrielli** - *Theoretical Foundation* - [WebSite](http://cs.unibo.it/~gabbri)
* **Prof. Ivan Lanese** - *Theoretical Foundation* - [WebSite](http://cs.unibo.it/~ilanese)
* **Post Doc. Saverio Giallorenzo** - *Theoretical Foundation* - [WebSite](http://cs.unibo.it/~giallor)
* **PhD Candidate Stefano Pio Zingaro** - *Code and Maintenance* - [WebSite](http://cs.unibo.it/~stefanopio.zingaro)

## License

This project is licensed under the GNU v.3 License - see the [LICENSE](LICENSE.md) file for details

## Acknowledgements

* [hirotakaster](https://github.com/hirotakaster) - who accepted all of the pull requests that I made to his library ;-).
* [klag](https://github.com/klag) - to useful insight on the Jolie orchestrator implementation.
