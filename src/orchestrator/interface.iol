type SpinType : void { .direction: string }
type WebType: void { .form: string }

interface iRobot {
  OneWay: go, spin, spinLeft, spinRight, inchino
  RequestResponse: index
}

