include "interface.iol"
include "deployment.ol"

inputPort Web {
    Location: "socket://localhost:9000"
    Protocol: http
    Interfaces: iRobot
}

execution{ concurrent }

main {

    [ index()( f ) {
        f = "
<!DOCTYPE html>
<html lang='it-IT'>
  <head>
    <meta charset='utf-8'>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>
    <script src='https://code.jquery.com/jquery-3.3.1.min.js' integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=' crossorigin='anonymous'></script>
    <script>$(document).ready(function(){$('#go').click(function(){$.ajax({url: 'go'});});});</script>
    <script>$(document).ready(function(){$('#spinLeft').click(function(){$.ajax({url: 'spinLeft'});});});</script>
    <script>$(document).ready(function(){$('#spinRight').click(function(){$.ajax({url: 'spinRight'});});});</script>
    <script>$(document).ready(function(){$('#inchino').click(function(){$.ajax({url: 'inchino'});});});</script>
  </head>
  <body>
    <div class='jumbotron text-center' style='background-color: #e0e1dc;'>
      <h1>Ciao! Io sono Ugly Robot ...</h1>
      <img src='http://ih0.redbubble.net/image.31027873.0566/fc,220x200,silver.u4.jpg' class='img-thumbnail'> 
      <p>... e questa pagina serve per farti provare i miei controlli:</p>
      <button id='go' type='button' class='btn btn-lg btn-primary'>VAI</button>
      <button id='spinLeft' type='button' class='btn btn-lg btn-primary'>GIRA A SINISTRA</button>
      <button id='spinRight' type='button' class='btn btn-lg btn-primary'>GIRA A DESTRA</button>
      <button id='inchino' type='button' class='btn btn-lg btn-primary'>FAI UN INCHINO</button>
    </div>
  </body>
</html>"
    }]
    [ go() ] {
        go@Robot_Coap()
    }
    [ inchino() ] {
        inchino@Robot_Coap()
    }
    [ spinLeft( request ) ] {
        spin@Robot_Coap( "left" )
    }
    [ spinRight( request ) ] {
        spin@Robot_Coap( "right" )
    }
}