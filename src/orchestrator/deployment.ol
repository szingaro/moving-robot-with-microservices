include "interface.iol"

outputPort Robot_Coap {
    Location: "datagram://192.168.44.130:5683"
    Protocol: coap
    Interfaces: iRobot
}

outputPort Broker {
    Location: "socket://localhost:1883"
    Protocol: mqtt
    Interfaces: iRobot
}

