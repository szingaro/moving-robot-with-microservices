#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <coap.h>
#include <MQTTClient.h>

void callback_response(CoapPacket &packet, IPAddress ip, int port);
void callback_go(CoapPacket &packet, IPAddress ip, int port);
void callback_spin(CoapPacket &packet, IPAddress ip, int port);

WiFiClient wifi_client;
MQTTClient mqtt_client;
WiFiUDP udp;
Coap coap(udp);

void callback_go(CoapPacket &packet, IPAddress ip, int port) {
  Serial.write("go()\n");
}

void callback_inchino(CoapPacket &packet, IPAddress ip, int port) {
  Serial.write("inchino()\n");
}

void callback_spin(CoapPacket &packet, IPAddress ip, int port) {
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = 0;
  String message(p);
  if (strcmp(p,"right") == 0) {
    Serial.write("spin(right)\n");
  }
  if (strcmp(p,"left") == 0){
    Serial.write("spin(left)\n");
  }
}

void callback_response(CoapPacket &packet, IPAddress ip, int port) {
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = 0;
}

void messageReceived(String & topic, String & payload)
{
  if (topic.equals("go"))
  {
    Serial.write("go()\n");
  }
  if (topic.equals("inchino"))
  {
    Serial.write("inchino()\n");
  }
  if (topic.equals("spin"))
  {
    if (payload.equals("left"))
    {
      Serial.write("spin(left)\n");
    }
    if (payload.equals("right"))
    {
      Serial.write("spin(right)\n");
    }
  }
}

void setup()
{
  Serial.begin(115200);
  WiFi.begin("LocalFocus", "imolap2018");
  
  mqtt_client.begin("192.168.44.128", 1883, wifi_client);
  mqtt_client.onMessage(messageReceived);
  
  connect();
  
  coap.server(callback_go, "go");
  coap.server(callback_spin, "spin");
  coap.server(callback_inchino, "inchino");
  coap.response(callback_response);
  coap.start();
}

void connect()
{
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
  }
  Serial.println(WiFi.localIP());
  String clientId = "ESP8266Client-";
  clientId += String(random(0xffff), HEX);
  while (!mqtt_client.connect(clientId.c_str())) {
    Serial.println(".");
    delay(1000);
  }
  mqtt_client.subscribe("go");
  mqtt_client.subscribe("spin");
  mqtt_client.subscribe("inchino");
}

void loop() {
  mqtt_client.loop();
  delay(10);
  if (!mqtt_client.connected())
  {
    connect();
  }
  delay(1000);
  coap.loop();
}
