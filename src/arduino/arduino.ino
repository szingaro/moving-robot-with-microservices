#include <string.h>
#include "Robot.h"
#include <SoftwareSerial.h>
#include <Servo.h>

// camera pins
int CAM_RX = 3;
int CAM_TX = 2;
SoftwareSerial swSer(CAM_TX,CAM_RX);

// servos pins
Servo servoHorizontal;
int SERVO_HORIZONTAL_PWM = 10;
Servo servoVertical;
int SERVO_VERTICAL_PWM = 9;
int val_min_hor = 15;
int val_max = 180;
int val_min_ver = 85;

// robot controller pins
int STBY = 8; 
#define LEFT_MOTOR 1
int PWMA = 6;
int AIN1 = 7; 
int AIN2 = 4; 
#define RIGHT_MOTOR 2
int PWMB = 5; 
int BIN1 = 12;
int BIN2 = 13;
Robot *robot;
char buff[64];
int idxBuff = 0;

void setup() {
  pinMode(STBY, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  Serial.begin(115200);
  swSer.begin(9600);
  servoHorizontal.attach(SERVO_HORIZONTAL_PWM);
  servoVertical.attach(SERVO_VERTICAL_PWM);
  robot = new Robot (STBY, PWMA, AIN1, AIN2, PWMB, BIN1, BIN2);
  robot->sendCommand (Robot::STOP, 0);
  memset(buff, 0, sizeof(buff));
  int val = 11;
  int val_hor = map(val, 0, 20, val_min_hor, val_max);
  int val_ver = map(val, 0, 20, val_min_ver, val_max);
  servoHorizontal.write(val_hor);
  servoVertical.write(val_ver);
}

void go(char *p) {
  robot->sendCommand (Robot::AHEAD, 150, 150);
  delay(1000);
  robot->sendCommand (Robot::STOP, 0);
}
void spin(char *p) {
  if (strncmp(p, "left", 4) == 0) {
    robot->sendCommand (Robot::SPIN_LEFT, 150);
  }
  else if (strncmp(p, "right", 5) == 0) {
    robot->sendCommand (Robot::SPIN_RIGHT, 150);
  }
  delay(1000);
  robot->sendCommand (Robot::STOP, 0);
}

void inchino(char *p) {
  int val = 100;
  int val_hor = map(val, 0, 20, val_min_hor, val_max);
  int val_ver = map(val, 0, 20, val_min_ver, val_max);
  servoHorizontal.write(val_hor);
  servoVertical.write(val_ver);
  val = 11;
  val_ver = map(val, 0, 20, val_min_ver, val_max);
  servoVertical.write(val_ver);
}

void manageBuff(char *b, int sizeb) {
  char *p = strchr(b, '(');
  if (p) {
    char *command = b;
    char *param = p+1;
    *p = 0;
    p = strchr(param, ')');
    if (p) {
      *p = 0;
      if (strncmp(command, "go", 2) == 0) {
        go(param);
      }
      else if (strncmp(command, "spin", 4) == 0) {
        spin(param);
      }
      else if (strncmp(command, "inchino", 7) == 0) {
        inchino(param);
      }
    }
  }
}

void loop() {
  int incomingByte; 
  while (Serial.available() > 0) {
    incomingByte = Serial.read();
    if (incomingByte == '\n') {
      manageBuff(buff, sizeof(buff));
      memset(buff, 0, sizeof(buff));
      idxBuff = 0;
    } else {
      buff[idxBuff] = incomingByte;
      idxBuff++;
    }
  }
}
