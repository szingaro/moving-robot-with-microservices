#include "Robot.h"



// motore A connesso tramite i pin del driver AIN01 e AIN02 -> sinistro
// motore B connesso tramite i pin del driver BIN01 e BIN02 -> destro



/*
 * costruttore che si occupa di inizializzare i pin per la comunicazione con il driver dei motori (e quindi della comunicazione con in motori del robot)
 * pin per lo standby dei motori = 10
 * pin per il controllo della velocità del motore A = 3
 * pin direzione motore A = 9
 * pin direzione motore A = 8
 * pin per il controllo della velocità del motore B = 5
 * pin direzione motore B = 11
 * pin direzione motore B = 12
 * inizializza il robot nello stato di STOP
 */
 /*
Robot::Robot ()
{
  // inizializzo le variabili private
  STBY = 10;
  PWMA = 3;
  AIN01 = 9;
  AIN02 = 8;
  PWMB = 5;
  BIN01 = 11;
  BIN02 = 12;
  TURNING_RATE = 50;
  MIN_SPEED = 0;
  MAX_SPEED = 255;
  RIGHT_OFFSET = 0;
  LEFT_OFFSET = 0;
  
  // inizializzo i pin
  pinMode (STBY, OUTPUT);
  pinMode (PWMA, OUTPUT);
  pinMode (AIN01, OUTPUT);
  pinMode (AIN02, OUTPUT);
  pinMode (PWMB, OUTPUT);
  pinMode (BIN01, OUTPUT);
  pinMode (BIN02, OUTPUT);
  
  // inizializzo i motori nello stato di stop
  sendCommand (STOP, 0);
}
*/


/*
 * costruttore che si occupa di inizializzare i pin per la comunicazione con il driver dei motori (e quindi della comunicazione con in motori del robot)
 * inizializza il robot nello stato di STOP
 *
 * param stby - pin per lo standby dei motori
 * param pwma - pin per il controllo della velocità del motore A
 * param ain01 - pin direzione motore A
 * param ain02 - pin direzione motore A
 * param pwmb - pin per il controllo della velocità del motore B
 * param bin01 - pin direzione motore B
 * param bin02 - pin direzione motore B
 */
Robot::Robot (int stby, int pwma, int ain01, int ain02, int pwmb, int bin01, int bin02)
{
  // inizializzo le variabili private
  STBY = stby;
  PWMA = pwma;
  AIN01 = ain01;
  AIN02 = ain02;
  PWMB = pwmb;
  BIN01 = bin01;
  BIN02 = bin02;
  TURNING_RATE = 50;
  MIN_SPEED = 0;
  MAX_SPEED = 255;
  RIGHT_OFFSET = 0;
  LEFT_OFFSET = 0;
  
  // inizializzo i pin
  pinMode (STBY, OUTPUT);
  pinMode (PWMA, OUTPUT);
  pinMode (AIN01, OUTPUT);
  pinMode (AIN02, OUTPUT);
  pinMode (PWMB, OUTPUT);
  pinMode (BIN01, OUTPUT);
  pinMode (BIN02, OUTPUT);
  
  // inizializzo i motori nello stato di stop
  sendCommand (STOP, 0);
}

/*
 * funzione che si occupa di eseguire il comando in ingresso
 * se la velocità è fuori dall'intervallo, automaticamente setta la velocità minima o massima
 */
void Robot::sendCommand (Command command, int speed_left, int speed_right) {
  if(command == AHEAD) {
    ahead (speed_left, speed_right);
  }
  else if (command == BACK) {
    back (speed_left, speed_right);
  }
  else {
    stop ();
  }
}


/*
 * funzione che si occupa di eseguire il comando in ingresso
 * se la velocità è fuori dall'intervallo, automaticamente setta la velocità minima o massima
 */
void Robot::sendCommand (Command command, int speed)
{
  if (speed < 0)
    speed = 0;
  else if (speed > 255)
    speed = 255;
  
  switch (command)
  {
    case AHEAD_LEFT:
    {
      aheadLeft (speed);
      break;
    }
    
    case AHEAD:
    {
      ahead (speed);
      break;
    }
    
    case AHEAD_RIGHT:
    {
      aheadRight (speed);
      break;
    }
    
    case LEFT:
    {
      left (speed);
      break;
    }
    
    case SPIN_LEFT:
    {
      spinLeft (speed);
      break;
    }
    
    case SPIN_RIGHT:
    {
      spinRight (speed);
      break;
    }
    
    case RIGHT:
    {
      right (speed);
      break;
    }
    
    case BACK_LEFT:
    {
      backLeft (speed);
      break;
    }
    
    case BACK:
    {
      back (speed);
      break;
    }
    
    case BACK_RIGHT:
    {
      backRight (speed);
      break;
    }
    
    // STOP
    default:
    {
      stop ();
    }
  }
}



/*
 * funzione che si occupa di settare la differenza di velocità (in percentuale) tra i due motori nelle curve
 * il valore di default è 50
 *
 * param turningRate - la nuova percentuale
 */
void Robot::setTurningRate (int turningRate)
{
  if (turningRate < 0)
    TURNING_RATE = 0;
  else if (turningRate > 100)
    TURNING_RATE = 100;
  else
    TURNING_RATE = turningRate;
}



/*
 * funzione che si occupa di tornare il valore in percentuale di differenza di velocità tra i due motori nelle curve
 *
 * return - il valore in percentuale di differenza di velocità tra i due motori nelle curve
 */
int Robot::getTurningRate ()
{
  return TURNING_RATE;
}



/*
 * funzione che si occupa di tornare il valore minimo per la velocità dei motori del robot
 *
 * return - il valore minimo per la velocità dei motori del robot
 */
int Robot::getMinSpeed ()
{
  return MIN_SPEED;
}



/*
 * funzione che si occupa di tornare il valore massimo per la velocità dei motori del robot
 *
 * return - il valore massimo per la velocità dei motori del robot
 */
int Robot::getMaxSpeed ()
{
  return MAX_SPEED;
}



/*
 * ***************************************
 * * funzioni per il controllo del ROBOT *
 * ***************************************
 */



/*
 * funzione per muovere il robot avanti (curva a sinistra)
 */
void Robot::aheadLeft (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore destro
  motorRightOn (speed);
  // muovo avanti il motore sinistro con velocità in percentuale minore rispetto al destro (per curvare)
  motorLeftOn ((int) (speed*TURNING_RATE/100));
}



/*
 * funzione per muovere il robot avanti (con velocità differenti per destra e sinistra)
 */
void Robot::ahead (int speed_left, int speed_right)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore destro
  motorRightOn (speed_right + RIGHT_OFFSET);
  // muovo avanti il motore sinistro
  motorLeftOn (speed_left + LEFT_OFFSET);
}

/*
 * funzione per muovere il robot avanti
 */
void Robot::ahead (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore destro
  motorRightOn (speed + RIGHT_OFFSET);
  // muovo avanti il motore sinistro
  motorLeftOn (speed + LEFT_OFFSET);
}



/*
 * funzione per muovere il robot avanti (curva a destra)
 */
void Robot::aheadRight (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore sinistro
  motorLeftOn (speed);
  // muovo avanti il motore destro con velocità in percentuale minore rispetto al sinistro (per curvare)
  motorRightOn ((int) (speed*TURNING_RATE/100));
}



/*
 * funzione per muovere il robot a sinistra
 */
void Robot::left (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore destro
  motorRightOn (speed);
  // tengo fermo il motore sinistro
  motorLeftStop ();
}



/*
 * funzione per ruotare il robot a sinistra
 */
void Robot::spinLeft (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore destro
  motorRightOn (speed);
  // muovo indietro il motore sinistro
  motorLeftBack (speed);
}

/*
 * funzione per ruotare il robot a sinistra
 */
void Robot::spinRight (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore sinistro
  motorLeftOn (speed);
  // muovo indietro il motore destro
  motorRightBack (speed);
}



/*
 * funzione per muovere il robot a destra
 */
void Robot::right (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo avanti il motore sinistro
  motorLeftOn (speed);
  // tengo fermo il motore destro
  motorRightStop ();
}



/*
 * funzione per muovere il robot indietro (curva a sinistra)
 */
void Robot::backLeft (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo indietro il motore destro
  motorRightBack (speed);
  // muovo indietro il motore sinistro con velocità in percentuale minore rispetto al destro (per curvare)
  motorLeftBack ((int) (speed*TURNING_RATE/100));
}



/*
 * funzione per muovere il robot indietro (con valori differenti per sinistra e desctra)
 */
void Robot::back (int speed_left, int speed_right)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo indietro il motore destro
  motorRightBack (speed_right + RIGHT_OFFSET);
  // muovo indietro il motore sinistro
  motorLeftBack (speed_left + LEFT_OFFSET);
}


/*
 * funzione per muovere il robot indietro
 */
void Robot::back (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo indietro il motore destro
  motorRightBack (speed + RIGHT_OFFSET);
  // muovo indietro il motore sinistro
  motorLeftBack (speed + LEFT_OFFSET);
}



/*
 * funzione per muovere il robot indietro (curva a destra)
 */
void Robot::backRight (int speed)
{
  // disabilito lo standby dei motori
  setStandby (false);
  // muovo indietro il motore sinistro
  motorLeftBack (speed);
  // muovo indietro il motore destro con velocità in percentuale minore rispetto al sinistro (per curvare)
  motorRightBack ((int) (speed*TURNING_RATE/100));
}



/*
 * funzione per fermare il robot
 */
void Robot::stop ()
{
  // tengo fermo il motore destro
  motorRightStop ();
  // tengo fermo il motore sinistro
  motorLeftStop ();
  // abilito lo standby dei motori
  setStandby (true);
}



/*
 * **********************************************************
 * * funzioni per il controllo dei motori DESTRO e SINISTRO *
 * **********************************************************
 */



/*
 * funzione per fermare il motore destro
 */
void Robot::motorRightStop ()
{
  motorBStop ();
}



/*
 * funzione per ruotare avanti il motore destro
 */
void Robot::motorRightOn (int speed)
{
  motorBClockwise (speed);
}



/*
 * funzione per ruotare indietro il motore destro
 */
void Robot::motorRightBack (int speed)
{
  motorBCounterClockwise (speed);
}



/*
 * funzione per fermare il motore sinistro
 */
void Robot::motorLeftStop ()
{
  motorAStop ();
}



/*
 * funzione per ruotare avanti il motore sinistro
 */
void Robot::motorLeftOn (int speed)
{
  motorACounterClockwise (speed);
}



/*
 * funzione per ruotare indietro il motore sinistro
 */
void Robot::motorLeftBack (int speed)
{
  motorAClockwise (speed);
}



/*
 * **************************************************************
 * * funzioni per il controllo a basso livello dei motori A e B *
 * **************************************************************
 */



/*
 * funzione per fermare il motore A
 */
void Robot::motorAStop ()
{
  digitalWrite (AIN01, LOW);
  digitalWrite (AIN02, LOW);
}



/*
 * funzione per ruotare in senso orario il motore A
 */
void Robot::motorAClockwise (int speed)
{
  digitalWrite (AIN01, HIGH);
  digitalWrite (AIN02, LOW);
  analogWrite (PWMA, ((speed > 255) ? 255 : speed));
}



/*
 * funzione per ruotare in senso antiorario il motore A
 */
void Robot::motorACounterClockwise (int speed)
{
  digitalWrite (AIN01, LOW);
  digitalWrite (AIN02, HIGH);
  analogWrite (PWMA, ((speed > 255) ? 255 : speed));
}



/*
 * funzione per fermare il motore B
 */
void Robot::motorBStop ()
{
  digitalWrite (BIN01, LOW);
  digitalWrite (BIN02, LOW);
}



/*
 * funzione per ruotare in senso orario il motore B
 */
void Robot::motorBClockwise (int speed)
{
  digitalWrite (BIN01, HIGH);
  digitalWrite (BIN02, LOW);
  analogWrite (PWMB, ((speed > 255) ? 255 : speed));
}



/*
 * funzione per ruotare in senso antiorario il motore B
 */
void Robot::motorBCounterClockwise (int speed)
{
  digitalWrite (BIN01, LOW);
  digitalWrite (BIN02, HIGH);
  analogWrite (PWMB, ((speed > 255) ? 255 : speed));
}



/*
 * funzione per mettere in standby i motori
 */
void Robot::setStandby (boolean standby)
{
  // abilito lo standby
  if (standby)
    digitalWrite (STBY, LOW);
  // disabilito lo standby
  else
    digitalWrite (STBY, HIGH);
}
