#include "Arduino.h"



class Robot
{
  public:
    /* set of commands */
    enum Command
    {
      AHEAD_LEFT,
      AHEAD,
      AHEAD_RIGHT,
      LEFT,
      SPIN_LEFT,
      SPIN_RIGHT,
      RIGHT,
      BACK_LEFT,
      BACK,
      BACK_RIGHT,
      STOP
    };
    
    /* constructors */
    Robot (int, int, int, int, int, int, int);
    
    /* fourth level function: the INTERFACE */
    void sendCommand (Command, int);
    void sendCommand (Command, int, int);
    
    /* set the right wheel offset */
    void setRightWheelOffset (int wo) {RIGHT_OFFSET = wo;};
    /* set the left wheel offset */
    void setLeftWheelOffset (int lo) {LEFT_OFFSET = lo;};
    
    /* function that allows user to change the turning rate */
    void setTurningRate (int);
    int getTurningRate ();
    
    /* functions for the motors speed value */
    int getMinSpeed ();
    int getMaxSpeed ();
    
  public:
    int RIGHT_OFFSET;       // valore da sommare alla speed della ruota di destra
    int LEFT_OFFSET;        // valore da sommare alla speed della ruota di sinistra
  
  private:
    /* variables */
    int STBY;               // pin dello standby
    int PWMA;               // pin per il controllo della velocità del motore A
    int AIN01;              // pin direzione motore A
    int AIN02;              // pin direzione motore A
    int PWMB;               // pin per il controllo della velocità del motore B
    int BIN01;              // pin direzione motore B
    int BIN02;              // pin direzione motore B
    int TURNING_RATE;       // percentuale rispetto alla velocità dell'altro motore (per le curve)
    int MIN_SPEED;          // valore minimo di velocità
    int MAX_SPEED;          // valore massimo di velocità
    
    /* third level function: higher level ROBOT control */
    void aheadLeft (int);
    void ahead (int);
    void ahead (int, int);
    void aheadRight (int);
    void left (int);
    void spinLeft (int);
    void spinRight (int);
    void right (int);
    void backLeft (int);
    void back (int);
    void back (int, int);
    void backRight (int);
    void stop ();
    
    /* second level function: middle level motor RIGHT and LEFT control */
    void motorRightStop ();
    void motorRightOn (int);
    void motorRightBack (int);
    void motorLeftStop ();
    void motorLeftOn (int);
    void motorLeftBack (int);
    
    /* first level function: lower level motor A and B control */
    void motorAStop ();
    void motorAClockwise (int);
    void motorACounterClockwise (int);
    void motorBStop ();
    void motorBClockwise (int);
    void motorBCounterClockwise (int);
    void setStandby (boolean);
};
